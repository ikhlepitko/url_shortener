from sanic import Sanic
from sanic.log import logger
from sanic.response import empty

import validators

from db import Db, UrlMapping
import utils

app = Sanic('url_shortener', env_prefix='URL_SHORTENER_')
app.ctx.db = Db()


@app.listener('before_server_start')
async def before_server_start(app, loop):
    app.ctx.host_base = utils.get_app_config(app, 'HOST_NAME', 'http://localhost:5000')
    region = utils.get_app_config(app, 'DB_REGION', 'eu-north-1')
    override_endpoint = utils.get_app_config(app, 'DB_OVERRIDE_ENDPOINT', None)

    # Override endpoint can be used to connect to local test instance of the database
    if override_endpoint is not None:
        logger.warning(f'Using DB endpoint override: {override_endpoint}. This should only be used for testing.')

    await app.ctx.db.setup(region, override_endpoint)


@app.listener('before_server_stop')
async def before_server_stop(app, loop):
    await app.ctx.db.shutdown()


@app.route('/healthcheck', methods=['GET'])
async def healthcheck(request):
    return empty()


@app.route('/<short_path>', methods=['GET'])
async def redirect_url(request, short_path):
    url_mapping = await app.ctx.db.fetch_url_mapping_by_short_path(short_path)
    if url_mapping is None:
        return utils.make_error_response(404, f'Cannot find page for {short_path}, is that a correct short URL?')

    # Simply redirect using 303 HTTP status code (See Other)
    return empty(status=303, headers={'Location': url_mapping.url})


@app.route('/v1/short_url', methods=['PUT'])
async def create_short_url(request):
    original_url = request.json['url']

    # Validate input URL first
    foo = validators.url(original_url)
    if not foo:
        return utils.make_error_response(400, f'"{original_url}" is not a valid URL')

    # Use SHA256 hash of the original URL as a primary key value.
    # We could use original URL as the primary key, but DynamoDB doesn't support primary keys longer than 2048
    # characters. Input URL can be much longer than that.
    url_hash = utils.get_hash(original_url)

    # First check if provided URL already exists
    url_mapping = await app.ctx.db.fetch_url_mapping_by_id(url_hash)
    if url_mapping is not None:
        return utils.make_response_from_url_mapping(200, app.ctx.host_base, url_mapping)

    # Ensure that generated short path is unique, simply retry generating path while we find one that doesn't exist in
    # database. Fail if couldn't find unique path after 5 tries. Not very clever, but should do the job. Can be
    # slightly improved by randomizing generated string length to reduce chance of the collision.
    async def generate_unique_short_path():
        left_tries = 5
        while left_tries > 0:
            left_tries = left_tries - 1
            short_path = utils.generate_string(12)
            url_mapping = await app.ctx.db.fetch_url_mapping_by_short_path(short_path)
            if url_mapping is None:
                return short_path

    short_path = await generate_unique_short_path()
    if short_path is None:
        logger.error(f'Failed to generate unique short path for "{original_url}"')
        return utils.make_error_response(500, f'Could not generate short path for "{original_url}"')

    create_res = await app.ctx.db.create_url_mapping(url_hash, original_url, short_path)
    if create_res:
        # Mapping was successfully created and a new item was added to the database
        url_mapping = UrlMapping(url_hash, original_url, short_path)
        return utils.make_response_from_url_mapping(201, app.ctx.host_base, url_mapping)

    # Mapping was not created, probably another client just created another mapping for the same URL.
    # Try fetching mapping with the same URL hash.
    url_mapping = await app.ctx.db.fetch_url_mapping_by_id(url_hash)
    if url_mapping is not None:
        return utils.make_response_from_url_mapping(200, app.ctx.host_base, url_mapping)
    else:
        logger.error(f'Failed to create URL mapping for "{original_url}"')
        # This message probably can be more helpful
        return utils.make_error_response(500, f'Could not create short URL for "{original_url}"')

if __name__ == '__main__':
    single_worker = 'SINGLE_WORKER' in app.config
    app.run(host='0.0.0.0', port=5000, fast=not single_worker)
