import hashlib
import random
import string

from sanic.response import json


def get_app_config(app, config_name, default_value):
    if config_name in app.config:
        return app.config[config_name]

    return default_value


def make_short_url(url_base, short_path):
    return f'{url_base}/{short_path}'


def generate_string(length):
    characters = string.ascii_lowercase + string.digits
    result = ''.join(random.choice(characters) for i in range(length))
    return result


def get_hash(input_str):
    return hashlib.sha256(input_str.lower().encode('utf-8')).hexdigest()


def make_response_from_url_mapping(status, url_base, url_mapping):
    body = {
        'url': url_mapping.url,
        'short_path': url_mapping.short_path,
        'short_url': make_short_url(url_base, url_mapping.short_path),
    }
    return json(body, status=status, escape_forward_slashes=False)


def make_error_response(status, message):
    return json({'error_message': message}, status=status, escape_forward_slashes=False)
