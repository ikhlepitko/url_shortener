from aiohttp import ClientSession

from aiodynamo.client import Client
from aiodynamo.credentials import Credentials, StaticCredentials, Key
from aiodynamo.errors import ConditionalCheckFailed, ItemNotFound
from aiodynamo.expressions import F
from aiodynamo.http.aiohttp import AIOHTTP
from aiodynamo.models import Throughput, KeySchema, KeySpec, KeyType, GlobalSecondaryIndex, Projection, ProjectionType

from yarl import URL


class UrlMapping:
    def __init__(self, id, url, short_path):
        self.id = id
        self.url = url
        self.short_path = short_path


class Db:
    def __init__(self):
        pass

    async def setup(self, region, override_endpoint):
        self._session = ClientSession()
        creds = Credentials.auto()
        if override_endpoint is not None:
            creds = StaticCredentials(Key('', ''))
            override_endpoint = URL(override_endpoint)

        self._client = Client(AIOHTTP(self._session), creds, region, endpoint=override_endpoint)

        table = self._client.table('ShortUrl')

        # Create table if it doesn't exist. We use 'id' (hash value from original url) as primary key, 'url' attribute
        # stores the original URL and 'short_path' attribute stores generated short path for the mapped URL.
        # Also create 'ShortUrlLookup' index to fetch url mapping by short path.
        if not await table.exists():
            await table.create(
                Throughput(read=10, write=10),
                KeySchema(hash_key=KeySpec('id', KeyType.string)),
                gsis=[
                    GlobalSecondaryIndex(
                        'ShortUrlLookup',
                        KeySchema(hash_key=KeySpec('short_path', KeyType.string)),
                        Projection(ProjectionType.all),
                        Throughput(read=10, write=10),
                    ),
                ],
                wait_for_active=True,
            )

    async def shutdown(self):
        await self._session.close()

    async def create_url_mapping(self, id, url, short_path):
        table = self._client.table('ShortUrl')
        try:
            await table.put_item({'id': id, 'url': url, 'short_path': short_path}, condition=F('id').does_not_exist())
            return True
        except ConditionalCheckFailed:
            # Mapping for this URL already exists, don't override it
            return False

    async def fetch_url_mapping_by_id(self, id):
        table = self._client.table('ShortUrl')
        try:
            item = await table.get_item({'id': id})
        except ItemNotFound:
            return None

        if item:
            return UrlMapping(item['id'], item['url'], item['short_path'])

        return None

    async def fetch_url_mapping_by_short_path(self, short_path):
        table = self._client.table('ShortUrl')
        try:
            iter = table.query(F('short_path').equals(short_path), index='ShortUrlLookup')
        except ItemNotFound:
            return None

        async for item in iter:
            return UrlMapping(item['id'], item['url'], item['short_path'])
        return None
