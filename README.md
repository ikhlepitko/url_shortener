# URL Shortener

## Overview

A simple service that can be used to create short URLs. In order to create a short URL, issue a PUT request on `/v1/short_url` endpoint:
```
curl http://localhost:5000/v1/short_url -d '{"url": "https://stackoverflow.com/questions/417142/what-is-the-maximum-length-of-a-url-in-different-browsers"}' -X PUT
```

You will be provided with a short URL in the response, like this:
```
{
    "url":"https://stackoverflow.com/questions/417142/what-is-the-maximum-length-of-a-url-in-different-browsers",
    "short_path":"ovaorxpwc98a",
    "short_url":"http://localhost:5000/ovaorxpwc98a"
}
```

You can then use provided link instead of the long URL. For example, navigating to `http://localhost:5000/ovaorxpwc98a` in the browser will get you redirected to `https://stackoverflow.com/questions/417142/what-is-the-maximum-length-of-a-url-in-different-browsers`.

## Deployment
It should be easy to deploy this service to AWS using image built from the provided Dockerfile.

## Starting the service

### Using docker-compose

The easiest way to start the service is by using docker-compose. First you need to create a docker network:
```
docker network create dynamodb-api
```

Once the network is created, you can start the service:
```
docker-compose up
```

This will first start a local instance of DynamoDB and then start the service on port 5000. You can then issue requests to create short URLs as described above.

### Runnig service directly

Alternatively, you can start the service directly from CLI. First you need to insall all the dependencies:
```
pip install -r requirements.txt
```

Then you need to start local DynamoDB instance:
```
docker run -d -p 8000:8000 --rm --name my-dynamodb amazon/dynamodb-local
```

Then you need to set `URL_SHORTENER_DB_OVERRIDE_ENDPOINT` environment variable to point to your local DynamoDB instance:
```
export URL_SHORTENER_DB_OVERRIDE_ENDPOINT=http://localhost:8000
```

Now you can start the service:
```
python3 url_shortener/server.py
```

By default the service will spawn multiple worker processes to match the number of CPUs available. To run with one worker only set `URL_SHORTENER_SINGLE_WORKER` environment variable.
```
URL_SHORTENER_SINGLE_WORKER=1 python3 url_shortener/server.py
```

### Linting

To run a linter check use:
```
flake8 url_shortener tests
```

### Testing

To run app tests use:
```
pytest -s tests/test_app.py
```

You can also run DB tests with this command:
```
pytest -s tests/test_app.py
```

Note that you need to have a local DynamoDB instance running for this test to pass. This test might be useful while developing DB API.
