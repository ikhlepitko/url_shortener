import os
import pytest
import sys
from yarl import URL

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../url_shortener')))

# This import needs to go after we alter sys.path
from url_shortener.db import Db  # noqa: E402


# This test can be ran against local DynamoDB instance. Useful for testing API while in development. Don't run on CI.
@pytest.mark.asyncio
async def test_basic_asgi_client():
    db = Db()
    endpoint = URL('http://localhost:8000')
    region = 'eu-north-1'
    await db.setup(region, endpoint)

    await db.create_url_mapping('hash1', 'http://test-url-1', 'abcd')
    await db.create_url_mapping('hash2', 'http://test-url-2', 'efgh')
    await db.create_url_mapping('hash3', 'http://test-url-3', 'ijkl')

    # Check that fetching random item works
    url_mapping = await db.fetch_url_mapping_by_id('hash3')
    assert url_mapping.id == 'hash3'
    assert url_mapping.url == 'http://test-url-3'
    assert url_mapping.short_path == 'ijkl'

    # Make sure we don't override previously created item
    res = await db.create_url_mapping('hash3', 'http://test-url-3', 'foo')
    assert res is False
    url_mapping = await db.fetch_url_mapping_by_id('hash3')
    assert url_mapping.short_path == 'ijkl'

    # Check that fetching non-existing mapping returns None
    url_mapping = await db.fetch_url_mapping_by_id('hash9')
    assert url_mapping is None

    # Check that fetching item by short path works
    url_mapping = await db.fetch_url_mapping_by_short_path('efgh')
    assert url_mapping.id == 'hash2'
    assert url_mapping.url == 'http://test-url-2'
    assert url_mapping.short_path == 'efgh'

    # Check that fetching item by non-existing short path returns None
    url_mapping = await db.fetch_url_mapping_by_short_path('bar')
    assert url_mapping is None

    await db.shutdown()
