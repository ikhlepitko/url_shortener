import os
import json
import pytest
import sys

from sanic import Sanic

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../url_shortener')))

# These imports need to go after we alter sys.path, and app needs to be imported to register Sanic client
from url_shortener.server import app  # noqa: E402, F401
from url_shortener.db import UrlMapping  # noqa: E402


# Simple class for mocking database API
class MockDb:
    def __init__(self):
        self.items = {}

    async def setup(self, region, endpoint):
        pass

    async def shutdown(self):
        pass

    async def create_url_mapping(self, id, url, short_path):
        if id in self.items:
            return False

        self.items[id] = (url, short_path)
        return True

    async def fetch_url_mapping_by_id(self, id):
        if id not in self.items:
            return None

        item = self.items[id]
        return UrlMapping(id, item[0], item[1])

    async def fetch_url_mapping_by_short_path(self, short_path):
        for id in self.items:
            item = self.items[id]
            if item[1] == short_path:
                return UrlMapping(id, item[0], item[1])

        return None


@pytest.fixture
def test_app():
    a = Sanic.get_app('url_shortener')
    a.ctx.db = MockDb()
    return a


@pytest.mark.asyncio
async def test_create_short_url(test_app):
    original_url = 'http://example.com'
    request, response = await test_app.asgi_client.put('/v1/short_url', data=json.dumps({'url': original_url}))

    assert response.status == 201
    assert response.json.get('url') == original_url
    assert response.json.get('short_url') != ''
    assert response.json.get('short_path') != ''


@pytest.mark.asyncio
async def test_create_same_url_twice(test_app):
    request, response = await test_app.asgi_client.put('/v1/short_url', data=json.dumps({'url': 'http://example.com'}))
    assert response.status == 201
    first_short_url = response.json.get('short_url')
    assert first_short_url != ''

    request, response = await test_app.asgi_client.put('/v1/short_url', data=json.dumps({'url': 'http://example.com'}))
    assert response.status == 200
    second_short_url = response.json.get('short_url')
    assert second_short_url != ''

    assert first_short_url == second_short_url


@pytest.mark.asyncio
async def test_create_with_malformed_url(test_app):
    request, response = await test_app.asgi_client.put('/v1/short_url', data=json.dumps({'url': 'http://example'}))
    assert response.status == 400
    assert response.json.get('error_message') != ''


@pytest.mark.asyncio
async def test_redirect(test_app):
    original_url = 'http://example.com'
    request, response = await test_app.asgi_client.put('/v1/short_url', data=json.dumps({'url': original_url}))
    assert response.status == 201
    short_path = response.json.get('short_path')

    request, response = await test_app.asgi_client.get(f'/{short_path}')
    assert response.status == 303
    assert response.headers['Location'] == original_url

    request, response = await test_app.asgi_client.get('/foobar')
    assert response.status == 404
    assert response.json.get('error_message') != ''


@pytest.mark.asyncio
async def test_healthcheck(test_app):
    request, response = await test_app.asgi_client.get('/healthcheck')
    assert response.status == 204
